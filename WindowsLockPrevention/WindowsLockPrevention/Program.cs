﻿using System;
using Microsoft.Win32;
using System.Windows.Forms;

namespace WindowsLockPrevention
{
    public class WindowsLockPreventionApp : Form
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.Run(new WindowsLockPreventionApp());
        }

        private NotifyIcon trayIcon;
        private ContextMenu trayMenu;
        private FormSettings formSettings;

        public WindowsLockPreventionApp()
        {
            GenerateTrayIcon();

            if (Properties.Settings.Default.PreventLockOnStartup == true)
            {
                StartWindowsLockPrevention();
                trayIcon.ShowBalloonTip(2, Application.ProductName, "Preventing Windows Lock", ToolTipIcon.Info);
            }
        }

        private void GenerateTrayIcon()
        {
            trayMenu = new ContextMenu();

            MenuItem menuItemPreventWindowsLock = new MenuItem();
            menuItemPreventWindowsLock.Text = "Prevent Windows Lock";
            menuItemPreventWindowsLock.Name = "menuItemPreventWindowsLock";
            menuItemPreventWindowsLock.Click += OnPreventWindowsLock_Click;
            trayMenu.MenuItems.Add(menuItemPreventWindowsLock);

            MenuItem menuItemSettings = new MenuItem();
            menuItemSettings.Text = "Settings";
            menuItemSettings.Name = "menuItemSettings";
            menuItemSettings.Click += OnSettings_Click;
            trayMenu.MenuItems.Add(menuItemSettings);

            MenuItem menuItemExit = new MenuItem();
            menuItemExit.Text = "Exit";
            menuItemExit.Click += OnExit_Click;
            trayMenu.MenuItems.Add(menuItemExit);

            trayIcon = new NotifyIcon
            {
                Text = "Windows Lock Prevention",
                Icon = Properties.Resources.WindowsLockPreventionIcon,

                ContextMenu = trayMenu,
                Visible = true
            };
        }

        private void StartWindowsLockPrevention()
        {
            WindowsLockPreventionTimer.SetInterval(Properties.Settings.Default.KeepAliveInterval);
            WindowsLockPreventionTimer.StartTimer();
            trayMenu.MenuItems["menuItemPreventWindowsLock"].Checked = WindowsLockPreventionTimer.Enabled;
        }

        private void StopWindowsLockPrevention()
        {
            WindowsLockPreventionTimer.StopTimer();
            trayMenu.MenuItems["menuItemPreventWindowsLock"].Checked = WindowsLockPreventionTimer.Enabled;
        }

        protected override void OnLoad(EventArgs e)
        {
            Visible = false;
            ShowInTaskbar = false;

            base.OnLoad(e);
        }

        private void OnPreventWindowsLock_Click(object sender, EventArgs e)
        {
            MenuItem menuItem = (MenuItem)sender;
            if (menuItem.Checked == true)
            {
                StopWindowsLockPrevention();
            } else
            {
                StartWindowsLockPrevention();
            }
        }

        private void OnSettings_Reload()
        {
            if (WindowsLockPreventionTimer.Enabled)
            {
                WindowsLockPreventionTimer.StopTimer();
                WindowsLockPreventionTimer.StartTimer();
            }

            RegistryKey rk = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            if (Properties.Settings.Default.StartAppOnLogin)
            { 
                rk.SetValue("WindowsLockPrevention", Application.ExecutablePath);
            }
            else
            {
                rk.DeleteValue("WindowsLockPrevention", false);
            }

        }

        private void OnSettings_Click(object sender, EventArgs e)
        {
            if (formSettings == null || formSettings.IsDisposed)
            {
                formSettings = new FormSettings();
            }
            formSettings.Saved += OnSettings_Reload;
            if (formSettings.Visible == false) {
                formSettings.ShowDialog();
            }
            else
            {
                formSettings.Focus();
            }
        }

        private void OnExit_Click(object sender, EventArgs e)
        {
            StopWindowsLockPrevention();
            trayIcon.Visible = false;
            Application.Exit();
        }
    }
}
