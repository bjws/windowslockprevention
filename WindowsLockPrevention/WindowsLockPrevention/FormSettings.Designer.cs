﻿namespace WindowsLockPrevention
{
    partial class FormSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblKeepaliveInterval = new System.Windows.Forms.Label();
            this.numericKeepaliveInterval = new System.Windows.Forms.NumericUpDown();
            this.lblPreventLockOnStartup = new System.Windows.Forms.Label();
            this.chkPreventLockOnStartup = new System.Windows.Forms.CheckBox();
            this.chkStartAppOnLogin = new System.Windows.Forms.CheckBox();
            this.lblStartAppOnLogin = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericKeepaliveInterval)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(202, 112);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(121, 112);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblKeepaliveInterval
            // 
            this.lblKeepaliveInterval.AutoSize = true;
            this.lblKeepaliveInterval.Location = new System.Drawing.Point(13, 13);
            this.lblKeepaliveInterval.Name = "lblKeepaliveInterval";
            this.lblKeepaliveInterval.Size = new System.Drawing.Size(136, 13);
            this.lblKeepaliveInterval.TabIndex = 2;
            this.lblKeepaliveInterval.Text = "Keepalive interval (minutes)";
            // 
            // numericKeepaliveInterval
            // 
            this.numericKeepaliveInterval.Location = new System.Drawing.Point(157, 13);
            this.numericKeepaliveInterval.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numericKeepaliveInterval.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericKeepaliveInterval.Name = "numericKeepaliveInterval";
            this.numericKeepaliveInterval.Size = new System.Drawing.Size(120, 20);
            this.numericKeepaliveInterval.TabIndex = 3;
            this.numericKeepaliveInterval.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblPreventLockOnStartup
            // 
            this.lblPreventLockOnStartup.AutoSize = true;
            this.lblPreventLockOnStartup.Location = new System.Drawing.Point(13, 41);
            this.lblPreventLockOnStartup.Name = "lblPreventLockOnStartup";
            this.lblPreventLockOnStartup.Size = new System.Drawing.Size(126, 13);
            this.lblPreventLockOnStartup.TabIndex = 4;
            this.lblPreventLockOnStartup.Text = "Prevent lock on app start";
            // 
            // chkPreventLockOnStartup
            // 
            this.chkPreventLockOnStartup.AutoSize = true;
            this.chkPreventLockOnStartup.Location = new System.Drawing.Point(157, 41);
            this.chkPreventLockOnStartup.Name = "chkPreventLockOnStartup";
            this.chkPreventLockOnStartup.Size = new System.Drawing.Size(15, 14);
            this.chkPreventLockOnStartup.TabIndex = 5;
            this.chkPreventLockOnStartup.UseVisualStyleBackColor = true;
            // 
            // chkStartAppOnLogin
            // 
            this.chkStartAppOnLogin.AutoSize = true;
            this.chkStartAppOnLogin.Location = new System.Drawing.Point(157, 65);
            this.chkStartAppOnLogin.Name = "chkStartAppOnLogin";
            this.chkStartAppOnLogin.Size = new System.Drawing.Size(15, 14);
            this.chkStartAppOnLogin.TabIndex = 7;
            this.chkStartAppOnLogin.UseVisualStyleBackColor = true;
            // 
            // lblStartAppOnLogin
            // 
            this.lblStartAppOnLogin.AutoSize = true;
            this.lblStartAppOnLogin.Location = new System.Drawing.Point(13, 65);
            this.lblStartAppOnLogin.Name = "lblStartAppOnLogin";
            this.lblStartAppOnLogin.Size = new System.Drawing.Size(66, 13);
            this.lblStartAppOnLogin.TabIndex = 6;
            this.lblStartAppOnLogin.Text = "Start at login";
            // 
            // FormSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(295, 147);
            this.Controls.Add(this.chkStartAppOnLogin);
            this.Controls.Add(this.lblStartAppOnLogin);
            this.Controls.Add(this.chkPreventLockOnStartup);
            this.Controls.Add(this.lblPreventLockOnStartup);
            this.Controls.Add(this.numericKeepaliveInterval);
            this.Controls.Add(this.lblKeepaliveInterval);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormSettings";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.FormSettings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericKeepaliveInterval)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblKeepaliveInterval;
        private System.Windows.Forms.NumericUpDown numericKeepaliveInterval;
        private System.Windows.Forms.Label lblPreventLockOnStartup;
        private System.Windows.Forms.CheckBox chkPreventLockOnStartup;
        private System.Windows.Forms.CheckBox chkStartAppOnLogin;
        private System.Windows.Forms.Label lblStartAppOnLogin;
    }
}