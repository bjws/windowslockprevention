﻿using System;
using System.Windows.Forms;

namespace WindowsLockPrevention
{
    public partial class FormSettings : Form
    {
        public delegate void SavedEventHandler();
        public event SavedEventHandler Saved;

        public FormSettings()
        {
            InitializeComponent();
        }

        public void OnSaved()
        {
            var handler = this.Saved;

            if (handler != null)
            {
                handler();
            }
        }

        private void FormSettings_Load(object sender, EventArgs e)
        {
            this.Icon = Properties.Resources.WindowsLockPreventionIcon;
            numericKeepaliveInterval.Value = Properties.Settings.Default.KeepAliveInterval;
            chkPreventLockOnStartup.Checked = Properties.Settings.Default.PreventLockOnStartup;
            chkStartAppOnLogin.Checked = Properties.Settings.Default.StartAppOnLogin;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.KeepAliveInterval = (int)numericKeepaliveInterval.Value;
            Properties.Settings.Default.PreventLockOnStartup = chkPreventLockOnStartup.Checked;
            Properties.Settings.Default.StartAppOnLogin = chkStartAppOnLogin.Checked;
            Properties.Settings.Default.Save();
            this.OnSaved();
            this.Dispose();
        }
    }
}
