﻿using System.Timers;
using System.Runtime.InteropServices;


namespace WindowsLockPrevention
{
    static class WindowsLockPreventionTimer
    {
        internal static class SystemState
        {
            // Import SetThreadExecutionState Win32 API and all flags
            [DllImport("kernel32.dll")]
            public static extern uint SetThreadExecutionState(uint esFlags);

            public const uint ES_SYSTEM_REQUIRED = 0x00000001;
            public const uint ES_DISPLAY_REQUIRED = 0x00000002;
            public const uint ES_USER_PRESENT = 0x00000004;
            public const uint ES_AWAYMODE_REQUIRED = 0x00000040;
            public const uint ES_CONTINUOUS = 0x80000000;
        }

        private static Timer lockPreventionTimer = new System.Timers.Timer();
        public static bool Enabled {
            get { return lockPreventionTimer.Enabled; }
        }

        static WindowsLockPreventionTimer()
        {
            lockPreventionTimer.Elapsed += new ElapsedEventHandler(LockPreventionTimerTimedEvent);
        }

        public static void SetInterval(int interval)
        {
            lockPreventionTimer.Interval = interval * 60 * 1000;
        }

        private static void LockPreventionTimerTimedEvent(object source, ElapsedEventArgs e)
        {
            SendKeepAlive();
        }

        public static void StartTimer()
        {
            lockPreventionTimer.Enabled = true;
        }

        public static void StopTimer()
        {
            lockPreventionTimer.Enabled = false;
        }

        private static void SendKeepAlive()
        {
            SystemState.SetThreadExecutionState(
                (
                    SystemState.ES_CONTINUOUS |
                    SystemState.ES_DISPLAY_REQUIRED |
                    SystemState.ES_SYSTEM_REQUIRED |
                    SystemState.ES_AWAYMODE_REQUIRED
                )
            );
        }

        private static void ResetSystemState()
        {
            SystemState.SetThreadExecutionState(SystemState.ES_CONTINUOUS);
        }
    }
}
